"""learnshell URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, re_path

from test.views import ListingView, Login, Logout, BadView, ThreadCreateView, ThreadUpdateView, \
	ThreadDeleteView, ThreadDetailFormView, UpdateCommentView, DeleteCommentView, LikeView

urlpatterns = [
	path('admin/', admin.site.urls),
	path('', ListingView.as_view(), name="listing"),
	path('login/', Login.as_view(), name="login"),
	path('logout/', Logout.as_view(), name="logout"),
	path('create_thread/', ThreadCreateView.as_view(), name="create"),
	path('update_thread/<int:pk>', ThreadUpdateView.as_view(), name="update"),
	path('delete_thread/<int:pk>', ThreadDeleteView.as_view(), name="delete"),
	path('detail_thread/<int:pk>', ThreadDetailFormView.as_view(), name="detail-thread"),
	path('update_comment/<int:pk>', UpdateCommentView.as_view(), name="form-update-comment"),
	path('delete_comment/<int:pk>', DeleteCommentView.as_view(), name="form-delete-comment"),
	path('like_comment/<int:pk>', LikeView.as_view(), name="like-comment"),
	re_path(r'^.*$', BadView.as_view()),
]
