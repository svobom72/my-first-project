Django Project Template
-----------------------

- This is a template with all the settings set so that it works with the Vagrant image.
- Contains a Vagrantfile template to make the installation of the development environment easier - see the Wiki for details.
- Also contains references to Django REST Framework which are commented out.

- All logs will automatically appear in the `logs` directory in the project root.

### Recommended project structure
- Templates shall be in the `templates` directory in the project root.
- Static files shall be in the `static` directory in the project root.

Those directories are not present, they need to be created manually.

### Initial configuration
- Clone this repository: `git clone git@gitlab.fit.cvut.cz:learnshell-new/django-project-template.git`.

- In the root directory of the project, make a copy of `Vagrantfile.dist` called `Vagrantfile` and edit it:
  - Replace `/path/to/your/shared/folder` with the absolute path to the project root (e. g. `/home/user/django-project-template`).
  - Run `vagrant up` and wait until the box is downloaded.
  - Run `vagrant ssh` to get the console of the virtual machine.

- Create a new project/repository on GitLab.

- Change the remote of the cloned repository so that it does not push to this location (the `master` branch of this project is protected to avoid accidental pushing): `git remote set-url origin [url]`, replacing `[url]` with the real URL of the newly created project.

- Push it for the first time with `git push -u origin master`.

### Features
- Custom logger: `from settings.settings import logger` and then `logger.info("message")` (or `warning`, `error`, `critical`).