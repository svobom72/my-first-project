from django.forms import ModelForm
from django.utils.timezone import now

from test.models import Thread, Comment


class ThreadForm(ModelForm):
	class Meta:
		model = Thread
		exclude = ["last_use", "author"]

	def __init__(self, **kwargs):
		self.author = kwargs.pop("author")
		super().__init__(**kwargs)

	def save(self, commit=True):
		obj = super().save(commit=False)
		obj.last_use = now()
		obj.author = self.author
		obj.save()
		return obj


class CommentForm(ModelForm):
	class Meta:
		model = Comment
		fields = ["text"]

	def __init__(self, **kwargs):
		self.user = kwargs.pop("user")
		self.thread = kwargs.pop("thread")
		super().__init__(**kwargs)

	def save(self, commit=True):
		obj = super().save(commit=False)
		obj.user = self.user
		obj.thread = self.thread
		obj.save()
		return obj
