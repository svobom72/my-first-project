from django.contrib.auth.models import User
from django.db.models import Model, TextField, CharField, ForeignKey, CASCADE, DateTimeField, ManyToManyField


class Thread(Model):
	title = CharField(max_length=50)
	author = ForeignKey(User, on_delete=CASCADE)
	text = TextField()

	last_use = DateTimeField(blank=True, null=True)

	def __str__(self):
		return self.title


class Comment(Model):
	thread = ForeignKey(Thread, on_delete=CASCADE)
	user = ForeignKey(User, on_delete=CASCADE)
	text = TextField()
	likes = ManyToManyField(User, blank=True, related_name='likes')

	def get_number_of_likes(self):
		return self.likes.count()