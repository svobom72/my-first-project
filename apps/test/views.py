from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import LoginView, LogoutView
from django.core.exceptions import PermissionDenied
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.views.generic import TemplateView, CreateView, UpdateView, DeleteView, DetailView
from django.views.generic.base import View
from django.views.generic.edit import FormMixin

from test.forms import ThreadForm, CommentForm
from test.models import Thread, Comment


class BadView(TemplateView):
	template_name = "bad_url.html"


class ListingView(LoginRequiredMixin, TemplateView):
	template_name = "list.html"

	def get_context_data(self, **kwargs):
		data = super().get_context_data(**kwargs)
		data["threads"] = Thread.objects.all().order_by("title")
		# It shows first four current user's threads order by title
		data["posts"] = Thread.objects.filter(author=self.request.user).order_by("title")[:4]
		# It shows first four current user's comments order by text
		data["comms"] = Comment.objects.filter(user=self.request.user).order_by("text")[:4]
		return data


class Login(LoginView):
	template_name = "login.html"


class Logout(LogoutView):
	next_page = reverse_lazy("login")


class ThreadCreateView(CreateView):
	model = Thread
	form_class = ThreadForm
	template_name = "simple_form.html"
	success_url = reverse_lazy("listing")

	def get_form_kwargs(self):
		data = super().get_form_kwargs()
		data["author"] = self.request.user
		return data


class LikeView(View):
	template_name = "thread_detail_form.html"

	def get(self, request, *args, **kwargs):
		likes = Comment.objects.get(pk=kwargs["pk"]).likes
		if likes.filter(id=request.user.id).exists():
			likes.remove(request.user)
		else:
			likes.add(request.user)
		return HttpResponseRedirect(reverse_lazy("detail-thread", kwargs={"pk": Comment.objects.get(pk=kwargs["pk"]).thread.pk}))

class ThreadUpdateView(UpdateView):
	model = Thread
	form_class = ThreadForm
	template_name = "simple_form.html"
	success_url = reverse_lazy("create")

	def get_form_kwargs(self):
		data = super().get_form_kwargs()
		data["author"] = self.request.user
		return data


class ThreadDeleteView(DeleteView):
	model = Thread
	success_url = reverse_lazy("listing")


class ThreadDetailFormView(FormMixin, DetailView):
	model = Thread
	form_class = CommentForm
	template_name = "thread_detail_form.html"

	def get_success_url(self):
		return reverse_lazy("detail-thread", kwargs={"pk": self.get_object().pk})

	def get_form_kwargs(self):
		data = super().get_form_kwargs()
		data["user"] = self.request.user
		data["thread"] = self.get_object()
		data["initial"] = self.request.POST
		return data

	def form_valid(self, form):
		form.save()
		return super().form_valid(form)

	def form_invalid(self, form):
		return super().form_invalid(form)

	def post(self, request, *args, **kwargs):
		self.object = self.get_object()
		form = self.get_form()
		if form.is_valid():
			return self.form_valid(form)
		else:
			return self.form_invalid(form)


class UpdateCommentView(UpdateView):
	model = Comment
	form_class = CommentForm
	template_name = "edit_comment.html"

	def dispatch(self, request, *args, **kwargs):
		if request.user != self.get_object().user:
			raise PermissionDenied
		return super().dispatch(request, *args, **kwargs)

	def get_form_kwargs(self):
		data = super().get_form_kwargs()
		data["user"] = self.request.user
		data["thread"] = self.get_object().thread
		return data

	def get_success_url(self):
		return reverse_lazy("detail-thread", kwargs={'pk': self.get_object().thread.pk})


class DeleteCommentView(DeleteView):
	model = Comment

	def get_success_url(self):
		return reverse_lazy("detail-thread", kwargs={'pk': self.get_object().thread.pk})
